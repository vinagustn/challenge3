import menu.ConstantPath;
import menu.Menu;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TestMenu {
    Menu menu  = new Menu();
    ConstantPath p = new ConstantPath();

    //create main for testing without scanner
    public int trueMain(int pilih) {
//        header();
        System.out.println("File yg dibaca dari " + p.PATH_READ);
        System.out.println("\nPilih menu : ");
        System.out.println("1. File txt yg menampilkan frekuensi modus");
        System.out.println("2. File txt yg menampilkan mean-median-modus");
        System.out.println("3. Generate 2 File txt");
        System.out.println("0. Exit");
        System.out.print("Pilih : ");
        try {
//            pilih = in.nextInt();
            switch (pilih) {
                case 0:
//                    System.exit(0);
                    break;
                case 1:
//                    header();
//                    rw.writeFreq(f.saveFreq, data);
//                    subMenu(pilih);
                    break;
                case 2:
//                    header();
//                    rw.writeM3(f.saveM3, data);
//                    subMenu(pilih);
                    break;
                case 3:
//                    header();
//                    rw.writeFreq(f.saveFreq, data);
//                    rw.writeM3(f.saveM3, data);
//                    subMenu(pilih);
                    break;
                default:
//                    back();
                    break;
            }
        } catch (InputMismatchException e) {
            System.err.println("Hanya boleh Integer");
            e.getMessage();
        }
        return pilih;
    }

    @Test
    @DisplayName("POSITIVE TEST MENU")
    void successMainMenuTest(){
        int input = 2;
        Assertions.assertEquals(input, trueMain(input));
    }

    @Test
    @DisplayName("NEGATIVE TEST MENU")
    void failedMainMenuTest(){
        int pilih = 'a';
        Assertions.assertThrows( InputMismatchException.class, () -> trueMain(pilih));
    }
}
