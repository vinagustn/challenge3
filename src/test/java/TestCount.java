import org.junit.jupiter.api.*;
import service.*;
import java.util.*;

public class TestCount {
    final String truePath = "src/main/resources/data_sekolah.csv";
    Counting count = new hitung();
    ReadWrite r = new implReadWriteFile();

    List<Integer> data = r.baca(truePath);
    double eMean = 8.318181818181818d;
    double eMedian = 8.0d;
    double eMode = 7.0d;


    @Test
    @DisplayName("POSITIVE TEST ALL MEAN,MED,MODE")
    void succesCount(){
        Assertions.assertEquals(eMean, count.mean(data));
        Assertions.assertEquals(eMedian, count.median(data));
        Assertions.assertEquals(eMode, count.modus(data));
    }

    @Test
    @DisplayName("NEGATIVE TEST ALL MEAN,MED,MODE")
    void failedCount(){
        Assertions.assertNull(count.mean(data));
        Assertions.assertNull(count.median(data));
        Assertions.assertNull(count.modus(data));
    }

    Integer[] keys = {5, 6, 7, 8, 9, 10};
    Integer[] values = {1, 2, 62, 42, 50, 41};
    Map<Integer, Integer> trueFreq = new HashMap<>();

    public Map<Integer, Integer> getTrueFreq() {
        for (int i=0; i< keys.length; i++){
            trueFreq.put(keys[i],values[i]);
        }
        return trueFreq;
    }
    //    Map<Integer, Integer> trueFreq = IntStream.range(0, key.length)
//            .boxed().collect(Collectors.toSet( key -> key[], i -> values[]));


    @Test
    @DisplayName("POSITIVE TEST MODE FREQUENCY")
    void successFreq(){
        Assertions.assertEquals(getTrueFreq(), count.frekuensi(data));
    }

    @Test
    @DisplayName("NEGATIVE TEST MODE FREQUENCY")
    void failedFreq(){
        Assertions.assertThrows(ArrayIndexOutOfBoundsException.class, () -> count.frekuensi(data));
    }
}
