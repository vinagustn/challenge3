import org.junit.jupiter.api.*;
import service.*;
import java.io.*;
import java.util.*;

public class ReadWriteTest {
    final String truePath = "src/main/resources/data_sekolah.csv";
    final String wrongPath = "src/main/resources/data_sekolah.txt";
    final String trueM3 = "src/main/resources/m3.txt";
    final String trueFreq = "src/main/resources/freq.txt";

    Integer[] expectedList= {5,
            6, 6,
            7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
            7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
            7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
            7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
            8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
            8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
            8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9,
            9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
            9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
            9, 9, 9, 9, 9, 9, 9,
            10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
            10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
            10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};

    ArrayList valueExpect = new ArrayList<>(Arrays.asList(expectedList));

    ReadWrite rw = new implReadWriteFile();

    @Test
    @DisplayName("POSITIVE READ FILE TEST")
    void successRead(){
        Assertions.assertEquals(valueExpect, rw.baca(truePath));
    }

    @Test
    @DisplayName("NEGATIVE READ FILE TEST")
    void failedRead(){
        Assertions.assertEquals(valueExpect, rw.baca(wrongPath));
    }

    @Test
    @DisplayName("POSITIVE WRITE MEAN MED MODE TEST")
    void successM3(){
        Assertions.assertDoesNotThrow(() ->  rw.writeM3(trueM3, rw.baca(truePath)));
    }

    @Test
    @DisplayName("NEGATIVE WRITE MEAN MED MODE TEST")
    void failedM3(){
        Assertions.assertThrows(IOException.class, () -> rw.writeM3(trueM3, rw.baca(truePath)));
    }

    @Test
    @DisplayName("POSITIVE WRITE FREQUENCY TEST")
    void successFreq(){
        Assertions.assertDoesNotThrow(() ->  rw.writeFreq(trueFreq, rw.baca(truePath)));
    }

    @Test
    @DisplayName("NEGATIVE WRITE FREQUENCY TEST")
    void failedFreq(){
        Assertions.assertThrows(IOException.class, () -> rw.writeFreq(trueFreq, rw.baca(truePath)));
    }
}
