package service;

import java.util.List;

public interface ReadWrite {
    List<Integer> baca(String readFile);
    void writeM3(String savePath, List<Integer> read);
    void writeFreq(String savePath, List<Integer> read);
}
