package service;

import menu.ConstantPath;

import java.io.*;
import java.util.*;

public class implReadWriteFile implements ReadWrite{

    Counting count = new hitung();
    ConstantPath path;
//    List<Integer> read = baca(path.PATH_READ);

    @Override
    public List<Integer> baca(String fileRead) {
        try{
            File file = new File(fileRead);
            FileReader read = new FileReader(file);
            BufferedReader br = new BufferedReader(read);

            String line = "";
            String[] arrTemp;
            List<Integer> data = new ArrayList<>();

            while ((line = br.readLine()) != null){
                arrTemp = line.split(";");
                for(int i=1; i<arrTemp.length; i++){
                    String temp = arrTemp[i];
                    Integer bantu = Integer.parseInt(temp);
                    data.add(bantu);
                }
            }
            br.close();
            Collections.sort(data);
            return data;
        } catch (IOException e) {
            System.err.println("Filenya ga ketemu bestii!!");
            e.getMessage();
        }
        return null;
    }

    @Override
    public void writeM3(String savePath, List<Integer> read) {
        try{
            File file = new File(savePath);
            if (file.createNewFile())
                System.out.println("File digenerate di "+savePath);
            else if (file.exists())
                System.out.println("File telah digenerate di "+savePath+"\nSilahkan reload");
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);

            bwr.write("Berikut Hasil Pengolahan Nilai\n");

            bwr.write("\nBerikut hasil sebaran nilai : ");
            bwr.write("\nMean   : "+count.mean(read));
            bwr.write("\nMedian : "+count.median(read));
            bwr.write("\nModus  : "+count.modus(read));

            bwr.flush();
            bwr.close();
        } catch (IOException e) {
            System.err.println("Something Wrong Here!");
            e.printStackTrace();
        }
    }

    @Override
    public void writeFreq(String savePath, List<Integer> read) {
        try{
            File file = new File(savePath);
            if (file.createNewFile())
                System.out.println("File digenerate di "+savePath);
            else
                System.out.println("File telah digenerate di "+savePath+"\nSilahkan reload");
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);

            Map<Integer,Integer> forRead = count.frekuensi(read);
            Set<Integer> key = forRead.keySet();

            bwr.write("Berikut Hasil Pengolahan Nilai\n");
            bwr.write("Nilai \t\t|"+"\t\tFrekuensi\n");
            for(Integer value : key)
                bwr.write(value+"\t\t\t|"+"\t\t\t"+forRead.get(value)+"\n");

            bwr.flush();
            bwr.close();
        } catch (IOException e) {
            System.err.println("Something Wrong Here!");
            e.printStackTrace();
        }
    }

}
