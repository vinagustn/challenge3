package service;

import menu.ConstantPath;

import java.util.*;

public class hitung implements Counting{

    @Override
    public Map<Integer, Integer> frekuensi(List<Integer> read) {
        try{
            Set<Integer> temp = new HashSet<>(read);
            Map<Integer, Integer> freq = new HashMap<>();

            for (Integer values : temp) {
                freq.put(values, Collections.frequency(read, values));
            }
            return freq;
        }catch (ArithmeticException | ArrayIndexOutOfBoundsException | NullPointerException e){
            System.out.println("Ada yg salah nih Guys!");
            e.getMessage();
        }
        return null;
    }

    @Override
    public double mean(List<Integer> read) {
        try {
            double sum = 0, temp;
            for(int i=0; i< read.size(); i++){
                temp = read.get(i);
                sum += temp;
            }
            return sum/read.size();
        }catch (ArithmeticException | ArrayIndexOutOfBoundsException | NullPointerException e){
            System.out.println("Ada yg salah nih Guys! ");
            e.getMessage();
        }
        return 0;
    }

    @Override
    public double median(List<Integer> read) {
        try{
            double median;
            int elemen = read.size();
            if(elemen % 2 == 0) {
                double avg = (read.get(elemen/2)+ read.get(elemen/2 -1));
                median = avg / 2;
            }else
                median = read.get(elemen/2);
            return median;
        }catch (ArithmeticException | ArrayIndexOutOfBoundsException | NullPointerException e){
            System.out.println("Ada yg salah nih Guys! ");
            e.getMessage();
        }
        return 0;
    }

    @Override
    public double modus(List<Integer> file) {
        try {
            int maxValue = 0, maxCount = 0;
            for(int i=0; i<file.size(); i++){
                int count = 0;
                for (int j=0; j<file.size(); j++){
                    if(file.get(j) == file.get(i)){
                        count++;
                    }
                }
                if(count > maxCount){
                    maxCount = count;
                    maxValue = file.get(i);
                }
            }
            return maxValue;
        }catch (ArithmeticException | ArrayIndexOutOfBoundsException | NullPointerException e){
            System.out.println("Ada yg salah nih Guys! ");
            e.getMessage();
        }
        return 0;
    }
}
