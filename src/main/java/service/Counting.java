package service;

import java.util.List;
import java.util.Map;

public interface Counting {
    Map<Integer, Integer> frekuensi(List<Integer> read);
    double mean(List<Integer> read);
    double median(List<Integer> read);
    double modus(List<Integer> read);
}
