package menu;

import com.sun.org.apache.bcel.internal.classfile.Constant;
import service.*;
import java.util.*;

public class Menu {
    Scanner in = new Scanner(System.in);
    ConstantPath f = new ConstantPath();
    ReadWrite rw = new implReadWriteFile();

//    int pilih, subPilih;
    List<Integer> data = rw.baca(f.PATH_READ);

    private void header() {
        System.out.println("--------------------------------------------");
        System.out.println("APLIKASI PENGOLAH NILAI SISWA");
        System.out.println("--------------------------------------------");
    }

    public int main(int pilih) {
        header();
        System.out.println("File yg dibaca dari " + f.PATH_READ);
        System.out.println("\nPilih menu : ");
        System.out.println("1. File txt yg menampilkan frekuensi modus");
        System.out.println("2. File txt yg menampilkan mean-median-modus");
        System.out.println("3. Generate 2 File txt");
        System.out.println("0. Exit");
        System.out.print("Pilih : ");
        try {
            pilih = in.nextInt();
            switch (pilih) {
                case 0:
                    System.exit(0);
                    break;
                case 1:
                    header();
                    rw.writeFreq(f.saveFreq, data);
                    subMenu(pilih);
                    break;
                case 2:
                    header();
                    rw.writeM3(f.saveM3, data);
                    subMenu(pilih);
                    break;
                case 3:
                    header();
                    rw.writeFreq(f.saveFreq, data);
                    rw.writeM3(f.saveM3, data);
                    subMenu(pilih);
                    break;
                default:
                    back();
                    break;
            }
        } catch (InputMismatchException e) {
            System.err.println("Hanya boleh Integer");
            e.getMessage();
        }
        return pilih;
    }

    private void back() {
        int pilih = 0;
        System.out.println("Out of Menu!");
        System.out.print("Type anything and press enter. ");
        String any = in.next();
        main(pilih);
    }

    private int subMenu(int subPilih) {
        System.out.println("\n1. Kembali ke menu utama");
        System.out.println("0. Exit");
        System.out.print("Pilih  : ");
        try {
            subPilih = in.nextInt();
            switch (subPilih) {
                case 0:
                    System.exit(0);
                    break;
                case 1:
                    main(subPilih);
                    break;
                default:
                    back();
                    break;
            }
        } catch (InputMismatchException e) {
            System.err.println("Input Harus Integer!");
            e.getMessage();
        }
        return subPilih;
    }
}
